# git-server-docker-stagit

<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Table of Contents**

- [git-server-docker-stagit](#git-server-docker-stagit)
    - [Description](#description)
    - [Mirrors](#mirrors)
    - [Credits](#credits)
    - [Prerequisites](#prerequisites)
        - [Necessary customization](#necessary-customization)
        - [Optional customization](#optional-customization)
        - [SSH keys](#ssh-keys)
        - [Building your image](#building-your-image)
    - [Usage](#usage)
        - [Run the container](#run-the-container)
        - [Check that container works with your SSH key](#check-that-container-works-with-your-ssh-key)
        - [Adding persistence to repo and html storage](#adding-persistence-to-repo-and-html-storage)
        - [Create a new repo](#create-a-new-repo)
        - [Clone your new empty repo and start working in it](#clone-your-new-empty-repo-and-start-working-in-it)
        - [Upload existing repo into the new empty repo](#upload-existing-repo-into-the-new-empty-repo)
        - [Adding web frontend](#adding-web-frontend)
        - [Adding git daemon](#adding-git-daemon)
    - [K8S TODO](#k8s-todo)

<!-- markdown-toc end -->

## Description

A lightweight Git Server Docker image built with stagit running Alpine Linux.
Stagit produces static html with every push to a repo.
A web server container can be added to serve that html.
A git daemon container can be added to enable anonymous read-only cloning.

## Mirrors

[https://git.andersuno.nu/git-server-docker-stagit/log.html](https://git.andersuno.nu/git-server-docker-stagit/log.html "git.andersuno.nu")

[https://gitlab.com/andersuno/git-server-docker-stagit](https://gitlab.com/andersuno/git-server-docker-stagit "gitlab.com")

## Credits

Forked from jkarlosb [GitHub](https://github.com/jkarlosb/git-server-docker) and [Docker Hub](https://hub.docker.com/r/jkarlos/git-server-docker/)

![image git server docker](git-server-docker.jpg "git server docker")

## Prerequisites

### Necessary customization

* Modify line 10 + 11 in ./git-shell-commands/new-repo to echo correct information into "./url" and "./owner"

### Optional customization

* Url line can be deleted entirely if daemon container is not used
* Both url and owner lines can be deleted entirely if web server container is not used
  * Line 3 + 9 ./git-shell-commands/new-repo can also be deleted if web server container is not used
* Modify contents of ./resources
* Modify ./sshd_config
  * Allowing password or root authentication **not** recommended
  * By default, user git has insecure password 12345 but password authentication is disabled

### SSH keys

* Your SSH public key(s) need to be mounted in the container at /git-server/keys/
* Create folder ./keys and copy your id_ed25519.pub or other public key(s) into it

``` sh
mkdir ./keys
cp $HOME/.ssh/id_ed25519.pub ./keys/
```

### Building your image

``` sh
docker build -t localhost/git-server:test ./server_context
```

## Usage

### Run the container

Binding to localhost:2222 with keys volume mounted for SSH access.

``` sh
docker run -d -p 2222:22 -v ./keys:/git-server/keys localhost/git-server:test
```

### Check that container works with your SSH key

``` sh
ssh git@localhost -p 2222
```
```
...
Welcome to git-server-docker!
You've successfully authenticated, but I do not
provide interactive shell access.
...
```

### Adding persistence to repo and html storage

For persistence two more volumes need to be mounted.

``` sh
mkdir ./{repos,public-html}
docker run -d -p 2222:22 -v ./keys:/git-server/keys \
-v ./repos:/git-server/repos -v ./public-html:/git-server/public-html \
localhost/git-server:test
```

### Create a new repo

``` sh
ssh git@localhost -p 2222 new-repo testname testdescription
```

### Clone your new empty repo and start working in it

``` sh
git clone ssh://git@localhost:2222/git-server/repos/testname.git
```

### Upload existing repo into the new empty repo

``` sh
git remote add origin ssh://git@localhost:2222/git-server/repos/testname.git
git push origin master
```

### Adding web frontend

To serve the stagit generated html, start another container that mounts the
same public-html volume as the git-server, into the correct path.
Here I am using nginx from dockerhub binding to localhost:8080.

``` sh
docker run -d -p 8080:80 -v ./public-html:/usr/share/nginx/html nginx:stable-alpine
```

### Adding git daemon

To enable anonymous read-only cloning, build the image from
daemon_context and mount your repos folder when you run the container.
The git protocol uses port 9418.

``` sh
docker build -t localhost/git-daemon:test ./daemon_context
```

``` sh
docker run -d -p 9418:9418 -v ./repos:/git-server/repos localhost/git-daemon:test
```

Clone your previously created testrepo. The git daemon is configured to use
/git-server/repos as its root, so that part can be left out here.
Note that this is anonymous, unauthenticated and read only so you cannot push to
the repo, only pull. And so can anyone with your url.

``` sh
git clone git://localhost/testname.git
```

## K8S TODO
