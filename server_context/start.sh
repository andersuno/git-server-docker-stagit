#!/bin/sh

# If there is some public key in keys folder
# then it copies its contain in authorized_keys file
if [ "$(ls -A /git-server/keys/)" ]; then
  cd /home/git || return
  cat /git-server/keys/*.pub > .ssh/authorized_keys
  chown -R git:git .ssh
  chmod 700 .ssh
  chmod -R 600 .ssh/*
fi

# Checking permissions and fixing SGID bit in repos folder
# More info: https://github.com/jkarlosb/git-server-docker/issues/1
if [ -d /git-server/repos/ ]; then
  cd /git-server/repos || return
  chown -R git:git .
  chmod -R ug+rwX .
  find . -type d -exec chmod g+s '{}' +
fi
if [ -d /git-server/public-html/ ]; then
  cd /git-server/public-html || return
  cp ../resources/* . || return
  chown -R git:git .
  chmod -R ug+rwX .
  chmod -R o+r .
  find . -type d -exec chmod g+s '{}' +
fi

# -D flag avoids executing sshd as a daemon
# -e flag sends logs to stderr
/usr/sbin/sshd -D -e
